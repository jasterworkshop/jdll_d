﻿module main;

import std.stdio;
import std.socket;
import std.string;
import std.conv;
import std.file;
import std.process;
import std.math;

import Jaster.IO.BitConverter;
import Jaster.IO.BinaryFile;
import Jaster.Maths;
import Jaster.jpk;
import Jaster.library.loader;
import Jaster.jsocket;
import Jaster.IO.stringstream;
import Jaster.util;

StringStream SS;

//<Expression> ::= <Number> | '(' <Exprssion> <Operator> <Expression> ')'
//<Operator> ::= '+' | '-' | '/' | '*'

void main(string[] args)
{
	bool UnitSort(string one, string two)
	{
		return one > two;
	}
	
	string[] Test = [ "Zebra", "Alien", "Bacon" ];
	writeln(BubbleSort(Test, &UnitSort));
}

void AssertStringStream()
{
	StringStream SS = new StringStream("DIs30 70.50");
	assert(SS.Peek() == 'D');
	assert(SS.ReadChar() == 'D');
	assert(SS.ReadString(2) == "Is");
	assert(SS.ReadInt() == 30);
	SS.SkipBlanks();
	assert(SS.ReadDouble() == 70.50);
	SS.Close();
}